
#### 重要通知
>  **该项目从2020年免费维护至今，共计帮助三万多人考试。 但是最近收到群友反馈网上存在大量贩卖本仓库免费资源获取利益。**
>   **为了提供更优质的服务，我们决定从2024年3月10日起，将不再在公众号上免费分享资料。如果您需要最新的全套资料，[ 请点击获取最新全套资料 👈  ](https://91ke.cn/)。**  

#### 更新记录
- 24年5月考试课程已开始更新-机构同步（3月5号）
- 23年11月考试课程（综合、案例、论文）已开更新完毕（11月20号）
